CC=gcc
CFLAGS=-Wall -Wextra -pedantic -g
EXEC=trees

all : $(EXEC)

$(EXEC) : main.o libtrees.a
	$(CC) -o $@ $^

libtrees.a : trees.o main.o
	ar rcs $@ $^

trees.o : trees.c
	$(CC) -c $^ $(CFLAGS)

main.o : main.c
	$(CC) -c $^ $(CFLAGS)

clean :
	rm -rf *.o

mrproper : clean
	rm -rf $(EXEC)
