/*
 * Bréant Thomas
 */

#include <stdio.h>
#include "trees.h"


int main (int argc, char *argv[])
{
    Node * tree;
    mk_empty_tree(&tree);
    FILE* fd;
    if (argc > 1)
    {
        fd = fopen(argv[1], "r");
        printf("%s \n", argv[1]);
        if (fd != NULL)
        {
            load_tree(fd, &tree);
            print_tree(tree);
            printf("\n");
            int nodes, max, height;
            nodes = count_nodes(tree);
            printf("Le nombre de noeuds est : %d\n", nodes);
            max = get_max(tree);
            printf("La valeur max est : %d\n", max);
            height = tree_height(tree);
            printf("la hauteur de l'arbre est : %d\n", height);
            tree_search(tree, 30);
            del_node(tree, 30);
            print_tree(tree);
            printf("\n");
            is_tree_complete(tree);
            free_tree(&tree);
        }
    }
    return EXIT_SUCCESS;
}


