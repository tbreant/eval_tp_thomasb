/*
 * Bréant Thomas
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node ** ptr_tree, int val, struct node *left, struct node *right)
{
    if (*ptr_tree == NULL){
        *ptr_tree = malloc(sizeof(struct node));
        (*ptr_tree)->val = val;
        (*ptr_tree)->left = left;
        (*ptr_tree)->right = right;
    }
}


// initialize un empty tree
void mk_empty_tree(struct node ** ptr_tree)
{
	*ptr_tree = NULL;
}

// is tree empty?
bool is_empty(struct node *tree)
{
	return tree==NULL;
}

// is tree a leaf?
bool is_leaf(struct node *tree)
{
	return(!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x)
{
    if (*ptr_tree == NULL) //if tree is empty, need to create one
    {
        cons_tree(ptr_tree, x, NULL, NULL);
        return;
    }
    if (x<=(*ptr_tree)->val) 
        add(&(*ptr_tree)->left, x);
    else
        add(&(*ptr_tree)->right, x);

}

// print values of tree in descendant order
void print_tree(struct node *tree)
{
    if (!is_empty(tree))
    {
        print_tree(tree->right); //search the highest values
        printf("%d ", tree->val); // print values
        print_tree(tree->left);// search the lowest values
    }
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree)
{
    int nb;
    while(fscanf(fp, "%d", &nb) != EOF)
        add(ptr_tree, nb);
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree)
{
    struct node * tmp_tree = *ptr_tree;
    if (!is_empty(tmp_tree))
    {
        free_tree(&(tmp_tree->left));
        free_tree(&(tmp_tree->right));
        free(tmp_tree);
 }
 ptr_tree = NULL;
}

int count_nodes(struct node *tree)
{
    if (is_empty(tree))
        return 0;
    else
        return (1 + count_nodes(tree->left) + count_nodes(tree->right));
}

int get_max(struct node *tree)
{
    if (is_empty(tree))
        return 0;
    while(!is_empty(tree->right)) //comme les arbres sont triés par ordre croissant, il suffit d'aller tout à droite pour avoir la valeur max
        tree = tree->right;
    return (tree->val);
}

int max(int a, int b)
{
    if (a >= b)
        return a;
    else
        return b;
}

int tree_height(struct node *tree)
{
    if (is_empty(tree))
        return 0;
    else if (tree->left == NULL && tree->right == NULL)
        return 1;
    return (1 + max(tree_height(tree->left), tree_height(tree->right)));
}

bool tree_search(struct node *tree, int elem)
{
    if (is_empty(tree))
    {
        printf("Le noeud %d n'est pas dans l'arbre\n", elem);
        return false;
    }
    else
    {
        if (elem == tree->val)
        {
            printf("Le noeud %d est dans l'arbre\n", elem);
            return true;
        }
        else
        {
            if (elem < tree->val)
                return(tree_search(tree->left, elem));
            else
                return(tree_search(tree->right, elem));
        }
    }
}

bool del_node(struct node *tree, int elem)
{
    if (!tree_search(tree, elem))
        return false;
    else
    {
        if (elem == tree->val)
        {
            if (is_empty(tree->right))
            {
                tree->val = tree->left->val;
                free(tree->left);
                tree->left = NULL;
                return true;
            }
            else
            {
                tree->val = tree->right->val;
                free(tree->right);
                tree->right = NULL;
                return true;
            }
        }
        else
        {
            if (elem < tree->val)
                return(del_node(tree->left, elem));
            else
                return(del_node(tree->right, elem));
        }

    }
}

bool is_tree_complete(struct node * tree)
{
    if (is_empty(tree))
    {
        printf("L'arbre est complet\n");
        return true;
    }
    if (tree->left == NULL && tree->right == NULL)
        {
            printf("L'arbre est complet\n");
            return true;
        }
    if ((tree->left) && (tree->right))
        return(is_tree_complete(tree->left) && is_tree_complete(tree->right));
    else
    {
        printf("l'arbre n'est pas complet\n");
        return false;
    }
}

